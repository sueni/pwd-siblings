use std::ffi::OsString;
use std::path::PathBuf;

pub enum Course {
    Forward,
    Backward,
}

pub enum SortingMode {
    Abc(Course),
    Mtime(Course),
}

impl TryFrom<OsString> for SortingMode {
    type Error = ();

    fn try_from(value: OsString) -> Result<Self, Self::Error> {
        match value.to_str() {
            Some("abc-frw") => Ok(Self::Abc(Course::Forward)),
            Some("abc-bkwr") => Ok(Self::Abc(Course::Backward)),
            Some("mtime-frw") => Ok(Self::Mtime(Course::Forward)),
            Some("mtime-bkwr") => Ok(Self::Mtime(Course::Backward)),
            _ => Err(()),
        }
    }
}

pub enum Command {
    Up,
    Down,
    List,
}

pub struct Args {
    pub progress: bool,
    pub wrap: bool,
    pub srtmode: Option<SortingMode>,
    pub command: Command,
    pub directory: PathBuf,
}

pub const HELP: &str = "
USAGE:
  pwd-siblings [OPTIONS] COMMAND

COMMANDS:
  up, down, list

OPTIONS:
  --sorting <MODE>  [values: abc-frw, abc-bkwr, mtime-frw, mtime-bkwr]
  --dir <DIRECTORY>
  --progress
  --wrap
  ";

fn abort(msg: &str, code: i32) -> ! {
    eprint!("{}", msg);
    if !msg.ends_with('\n') {
        eprintln!();
    }
    std::process::exit(code)
}

pub fn parse_args() -> Result<Args, lexopt::Error> {
    use lexopt::prelude::*;

    let mut srtmode = None;
    let mut command = None;
    let mut directory = std::env::current_dir().unwrap_or_else(|_| abort("No PWD", 1));
    let mut progress = false;
    let mut wrap = false;
    let mut parser = lexopt::Parser::from_env();

    while let Some(arg) = parser.next()? {
        match arg {
            Long("sorting") => srtmode = parser.value()?.try_into().ok(),
            Long("dir") => directory = parser.value()?.parse()?,
            Long("progress") => progress = true,
            Long("wrap") => wrap = true,
            Value(val) => match val.as_os_str().to_str().unwrap() {
                "up" => command = Some(Command::Up),
                "down" => command = Some(Command::Down),
                "list" => command = Some(Command::List),
                _ => return Err(format!("invalid command: {}", val.into_string()?).into()),
            },

            Short('h') => abort(HELP, 0),
            _ => return Err(arg.unexpected()),
        }
    }

    let command = match command {
        None => return Err("need a command to run".into()),
        Some(cmd @ Command::List) => {
            if progress {
                abort("List command doesn't take the `--progress` option", 1);
            }
            if wrap {
                abort("List command doesn't take the `--wrap` option", 1);
            }
            cmd
        }
        Some(cmd @ Command::Up) => {
            if wrap {
                abort("Up command doesn't take the `--wrap` option", 1);
            }
            cmd
        }
        Some(cmd) => cmd,
    };

    if !directory.is_dir() {
        abort(&format!("Invalid directory: {}", directory.display()), 1);
    }

    Ok(Args {
        progress,
        wrap,
        srtmode,
        command,
        directory,
    })
}
