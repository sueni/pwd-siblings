use std::path::PathBuf;
use std::time::SystemTime;

pub struct DirItem {
    pub path: PathBuf,
    pub mtime: SystemTime,
}

impl DirItem {
    pub fn new(dir: PathBuf) -> std::io::Result<Self> {
        Ok(Self {
            mtime: dir.metadata()?.modified()?,
            path: dir,
        })
    }
}
