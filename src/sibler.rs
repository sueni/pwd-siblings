use crate::cli::SortingMode;
use crate::dir_item::DirItem;
use crate::sorter;
use std::io::{self, Write};
use std::os::unix::ffi::OsStrExt;
use std::path::Path;

pub struct Sibler<'a> {
    dir: &'a Path,
    pub dirs: Vec<DirItem>,
}

impl<'a> Sibler<'a> {
    pub fn new(dir: &'a Path, srtmode: Option<SortingMode>) -> std::io::Result<Self> {
        let parent = dir.parent().expect("can't get parent");
        let mut dirs = Vec::new();

        for entry in std::fs::read_dir(parent)? {
            let entry = entry?;
            let path = entry.path();
            if path.is_dir() {
                let dir_item = DirItem::new(path)?;
                dirs.push(dir_item);
            }
        }
        sorter::sort(&mut dirs, srtmode);
        Ok(Self { dirs, dir })
    }

    pub fn up(self, writer: &mut impl Write, progress: bool) -> io::Result<()> {
        let own_index = self.own_index();
        if own_index > 0 {
            if progress {
                self.report_progress(writer, own_index - 1)?;
            }
            writer.write_all(self.dirs[own_index - 1].path.as_os_str().as_bytes())?;
        }
        Ok(())
    }

    pub fn down(self, writer: &mut impl Write, progress: bool, wrap: bool) -> io::Result<()> {
        let own_index = self.own_index();
        if own_index + 1 < self.dirs.len() {
            if progress {
                self.report_progress(writer, own_index + 1)?;
            }
            writer.write_all(self.dirs[own_index + 1].path.as_os_str().as_bytes())?;
        } else if wrap && self.dirs.len() > 1 {
            if progress {
                self.report_progress(writer, 0)?;
            }
            writer.write_all(self.dirs[0].path.as_os_str().as_bytes())?;
        }
        Ok(())
    }

    pub fn list(self, writer: &mut impl Write) -> io::Result<()> {
        let siblings = self.dirs.iter().filter(|d| d.path != self.dir);
        for sibling in siblings {
            writer.write_all(sibling.path.as_os_str().as_bytes())?;
            writer.write_all(b"\n")?;
        }
        Ok(())
    }

    fn report_progress(&self, writer: &mut impl Write, idx: usize) -> io::Result<()> {
        write!(writer, "{}\0{}\0", idx + 1, self.dirs.len())?;
        Ok(())
    }

    fn own_index(&self) -> usize {
        self.dirs.iter().position(|d| d.path == self.dir).unwrap()
    }
}
