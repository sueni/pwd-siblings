use crate::cli::Course;
use crate::cli::SortingMode;
use crate::dir_item::DirItem;

pub fn sort(dirs: &mut [DirItem], srtmode: Option<SortingMode>) {
    if let Some(sm) = srtmode {
        match sm {
            SortingMode::Abc(Course::Forward) => sort_abc_forward(dirs),
            SortingMode::Abc(Course::Backward) => sort_abc_backward(dirs),
            SortingMode::Mtime(Course::Forward) => sort_mtime_forward(dirs),
            SortingMode::Mtime(Course::Backward) => sort_mtime_backward(dirs),
        }
    }
}

fn sort_abc_forward(dirs: &mut [DirItem]) {
    dirs.sort_unstable_by(|a, b| alphanumeric_sort::compare_path(&a.path, &b.path))
}

fn sort_abc_backward(dirs: &mut [DirItem]) {
    dirs.sort_unstable_by(|a, b| alphanumeric_sort::compare_path(&b.path, &a.path))
}

fn sort_mtime_forward(dirs: &mut [DirItem]) {
    dirs.sort_unstable_by(|a, b| a.mtime.cmp(&b.mtime));
}

fn sort_mtime_backward(dirs: &mut [DirItem]) {
    dirs.sort_unstable_by(|a, b| b.mtime.cmp(&a.mtime));
}
