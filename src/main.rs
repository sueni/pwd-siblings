#![feature(stdio_locked)]

mod cli;
mod dir_item;
mod sibler;
mod sorter;

use cli::Command;
use std::io;

fn main() -> std::io::Result<()> {
    let args = match cli::parse_args() {
        Ok(args) => args,
        Err(e) => {
            eprintln!("ERROR: {}\n{}", e, cli::HELP);
            std::process::exit(1);
        }
    };

    let sibler = sibler::Sibler::new(&args.directory, args.srtmode)?;
    let mut writer = io::stdout_locked();

    match args.command {
        Command::Up => sibler
            .up(&mut writer, args.progress)
            .expect("Failed to get an upper sibling"),
        Command::Down => sibler
            .down(&mut writer, args.progress, args.wrap)
            .expect("Failed ot get a lower sibling"),
        Command::List => sibler.list(&mut writer).expect("Failed to list siblings"),
    }
    Ok(())
}
